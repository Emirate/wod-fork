#Northshire Valley

#Blackrock Worg
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='49871';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='49871';

#Blackrock Spy
UPDATE `world`.`creature` SET `spawndist`='5' WHERE `id`='49874';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='49874';

#Blackrock Invader
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='42937';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='42937';

#other creatures were pathing correctly, so I did not touch them.
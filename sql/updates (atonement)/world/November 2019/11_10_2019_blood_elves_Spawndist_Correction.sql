# Spawndist corrections for Sunstrider Isle

#Mana Wyrm
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='15274';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15274';

#Springpaw Cub
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='15366';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15366';

#Springpaw Lynx
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='15372';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15372';

#Feral Tender
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='15294';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15294';

#Tender
UPDATE `world`.`creature` SET `spawndist`='10' WHERE `id`='15271';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15271';

#Arcane Wraith
UPDATE `world`.`creature` SET `spawndist`='5' WHERE `id`='15273';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15273';

#Tainted Arcane Wraith
UPDATE `world`.`creature` SET `spawndist`='5' WHERE `id`='15298';
UPDATE `world`.`creature` SET `MovementType`='1' WHERE `id`='15298';